package util;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

public class ApiUtils {

    //Sets Base URI
    public static void setBaseURI(){
        RestAssured.baseURI = "https://restcountries.eu/";
    }

    //Sets base path
    public static void setBasePath(String basePathTerm){
        RestAssured.basePath = basePathTerm;
    }

    //Reset Base URI (after test)
    public static void resetBaseURI (){
        RestAssured.baseURI = null;
    }

    //Reset base path
    public static void resetBasePath(){
        RestAssured.basePath = null;
    }

    // Set ContentType
    public static void setContentType (ContentType Type){
        given().contentType(Type);
    }

    // Set Query param
    public static void setQueryParam (){
        given().queryParam("fullText", true);
    }

    //Returns response by given path
    public static Response getResponsebyPath(String path) {
        return get(path);
    }
}

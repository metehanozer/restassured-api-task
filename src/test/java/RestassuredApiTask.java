import org.testng.Assert;
import org.testng.annotations.Test;

import util.ApiUtils;

import java.util.List;

public class RestassuredApiTask extends BaseTest {

    @Test
    public void T01_CheckStatusCode() {
        response = ApiUtils.getResponsebyPath("/turkiye");
        Assert.assertEquals(response.getStatusCode(),200, "Status Check Failed!");
        System.out.println("Response status code: " + response.getStatusCode());
    }

    @Test
    public void T02_CheckData() {
        response = ApiUtils.getResponsebyPath("/turkiye");

        // Response listenin uzunluğu 1 olmalı.
        List<String> listResponse = response.jsonPath().getList("$");
        System.out.println("Response data: " + listResponse);
        Assert.assertEquals(listResponse.size(),1, "Response list incorrect!");

        // Response list içinde bazı değerler kontrol ediliyor.
        Assert.assertEquals(response.jsonPath().getString("name[0]"),"Turkey", "name value incorrect!");
        Assert.assertEquals(response.jsonPath().getString("altSpellings[0]"),"[TR, Turkiye, Republic of Turkey, Türkiye Cumhuriyeti]", "altSpellings value incorrect!");
        Assert.assertEquals(response.jsonPath().getString("currencies[0][0]"),"[symbol:null, code:TRY, name:Turkish lira]", "currencies value incorrect!");
        Assert.assertEquals(response.jsonPath().getDouble("area[0]"),783562.0, "area value incorrect!");
    }
}

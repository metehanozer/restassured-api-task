import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import util.ApiUtils;

public class BaseTest {

    Response response = null;

    @BeforeClass
    public void setup() {
        ApiUtils.setBaseURI();
        ApiUtils.setBasePath("rest/v2/name");
        ApiUtils.setContentType(ContentType.JSON);
        ApiUtils.setQueryParam();
    }

    @AfterClass
    public void afterTest() {
        ApiUtils.resetBaseURI();
        ApiUtils.resetBasePath();
    }
}
